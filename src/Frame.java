import ntu.csie.oop13spring.*;

import java.util.*;
import java.util.concurrent.*;
import java.io.*;
import java.lang.*;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Frame extends JFrame{

  Map map = null;
  Controll controll = null;
  Container contentPane;
  GridBagLayout gridbag;
  GridBagConstraints c;
  POOArenaExt arena;

  public void finalize(){

  }

  public Frame(POOArenaExt a) {

    arena = a;     
    setTitle("Frame");
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setSize(896, 666);
    setLocationRelativeTo(null);
    setResizable(false);

    contentPane = getContentPane();
    gridbag = new GridBagLayout();
    c = new GridBagConstraints();

    contentPane.setLayout(gridbag);
    c.fill =GridBagConstraints.BOTH;   

    }
      public Map getMap(){return map;}

  public Controll getControll(){
    return controll;
  }

  public void setMap(Map m){
    map = m;

    c.gridx = 0; //x grid position
    c.gridy = 0; //y grid position
    c.weightx = 1;
    c.weighty = 0.72;

    map.setLayout(null);
    gridbag.setConstraints(map, c); //associate the label with a constraint object 
    contentPane.add(map); //add it to content pane

  }

  public void setControll(POOPet p,  MyWaitNotify ready){
    controll = new Controll(p, ready, arena);

    c.gridx = 0; //x grid position
    c.gridy = 1; //y grid position
    c.gridheight = 2;
    c.weightx = 1;
    c.weighty = 0.3;
    gridbag.setConstraints(controll, c); //associate the label with a constraint object 
    contentPane.add(controll); //add it to content pane
    
    contentPane.revalidate();
    setVisible(true);
  }

  public void removeControll(){
    contentPane.remove(controll);
    //contentPane.revalidate();
    //contentPane.repaint();
  }

  
}