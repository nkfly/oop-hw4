import ntu.csie.oop13spring.*;
//package ntu.csie.oop13spring;

import java.util.*;
import java.io.*;
import java.lang.*;

public class POOCoordinateExt extends ntu.csie.oop13spring.POOCoordinate{
	public POOCoordinateExt(int xPos, int yPos){
		x = xPos;
		y = yPos;
	}


	public int getX(){
		return x;

	}

	public int getY(){
		return y;
	}
	public boolean equals(POOCoordinate other){
		return (x == ((POOCoordinateExt)other).getX() && y == ((POOCoordinateExt)other).getY());
	}

}