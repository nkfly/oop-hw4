import ntu.csie.oop13spring.*;

import java.util.*;
import java.util.concurrent.*;
import java.io.*;
import java.lang.*;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MyWaitNotify{

  Object myMonitorObject = new Object();

  public void doWait(){
    synchronized(myMonitorObject){
      try{
        myMonitorObject.wait();
      } catch(InterruptedException e){
        System.err.println(e);
      }
    }
  }

  public void doNotify(){
    synchronized(myMonitorObject){
      myMonitorObject.notify();
    }
  }
}
