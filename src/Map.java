import ntu.csie.oop13spring.*;

import java.util.*;
import java.util.concurrent.*;
import java.io.*;
import java.lang.*;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Map extends JLayeredPane implements ActionListener{
	
  private POOPetExt [][] petStand;
  private JLabel [][] background;
  private MouseAdapter mouse;
  private ImageIcon originalFloor;
  private ImageIcon selectableFloor;
  private ImageIcon attackableFloor;
  private ImageIcon magicableFloor;
  private POOArenaExt arena;
  private int row = 7;
  private int column = 14;
  private POOPet [] pets;
  private POOPetExt extPet;
  private javax.swing.Timer moveTimer;
  private javax.swing.Timer attackTimer;
  private javax.swing.Timer magicTimer;
  private javax.swing.Timer hurtTimer;
  private int finalX;
  private int finalY;
  private WalkPose nextAnimationFrame;
  private MagicRegion mr;

  
  public void setCurrentExtPet(POOPet p){
    extPet = (POOPetExt)p;
  }


  public void actionPerformed(ActionEvent e) {
    if(moveTimer != null && moveTimer.isRunning()){
      remove(extPet.getIconLabel());
    
      if(extPet.getX() < finalX ) {
        extPet.walk(Direction.Right, nextAnimationFrame);
      }else if(extPet.getX() > finalX){
        extPet.walk(Direction.Left, nextAnimationFrame);
      }else if(extPet.getY() < finalY){
        extPet.walk(Direction.Down, nextAnimationFrame);
      }else if(extPet.getY() > finalY){
        extPet.walk(Direction.Up, nextAnimationFrame);
      }else {
        petStand[finalY/64][finalX/64] = extPet;
        extPet.turnAroundEnd();
        moveTimer.stop();
      }

      if(nextAnimationFrame == WalkPose.Still)nextAnimationFrame = WalkPose.LeftLeg;
      else if(nextAnimationFrame == WalkPose.LeftLeg)nextAnimationFrame = WalkPose.RightLeg;
      else if(nextAnimationFrame == WalkPose.RightLeg)nextAnimationFrame = WalkPose.Still;

    }else if(attackTimer != null && attackTimer.isRunning() && extPet.attackStart() == 0){
        attackTimer.stop();

        petStand[finalY/64][finalX/64].set_HP(petStand[finalY/64][finalX/64].get_HP()- extPet.get_ATK());
        arena.getStatus().notifyMainThread();

    }else if(magicTimer != null && magicTimer.isRunning() && extPet.magicStart(mr) == 0){
      //arena.getStatus().setIsMagicing(false);
        magicTimer.stop();
        arena.getStatus().setIsMagicing(false);
        hurtTimer.start();


        //petStand[finalY/64][finalX/64].set_HP(petStand[finalY/64][finalX/64].get_HP()- extPet.get_ATK());
        
        //arena.getStatus().notifyMainThread();

    }else if(hurtTimer != null && hurtTimer.isRunning() && extPet.hurtStart() == 0){
      //arena.getStatus().setIsMagicing(false);
        hurtTimer.stop();
        //hurtTimer.start();
        for(int i = finalY/64-mr.magicSpace; i <= finalY/64+mr.magicSpace;i++){
          for(int j = finalX/64-mr.magicSpace; j <= finalX/64+mr.magicSpace;j++){
            if( j < 0 || j >= column || i < 0 || i >= row )continue;

            if(petStand[i][j] != null && petStand[i][j] != extPet){
              // g2d.drawImage(extPet.getHurtImage(mr.atk), j*64-28, i*64-28, 128, 128, this);
              petStand[i][j].set_HP(petStand[i][j].get_HP()-mr.atk);
            }
              
          }
      }


        //petStand[finalY/64][finalX/64].set_HP(petStand[finalY/64][finalX/64].get_HP()- mr.atk);
        
        arena.getStatus().notifyMainThread();

    }

    repaint();
    }

    

    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D)g;
      if(moveTimer != null && moveTimer.isRunning()){

        g2d.drawImage(extPet.getSelfIcon().getImage(), extPet.getX(), extPet.getY(), 64, 64, this);
        
        

      }else if(attackTimer != null && attackTimer.isRunning()){

        g2d.drawImage(extPet.getAttackImage(extPet.get_ATK()), finalX-28, finalY-28, 128, 128, this);
      }else if(magicTimer != null && magicTimer.isRunning()){

        g2d.drawImage(extPet.getMagicImage(mr), finalX-32*(mr.magicSpace*4+1)+32, 32+finalY-32*(mr.magicSpace*4+1), 64*(mr.magicSpace*4+1), 64*(mr.magicSpace*4+1), this);
      }else if(hurtTimer != null && hurtTimer.isRunning()){
        for(int i = finalY/64-mr.magicSpace; i <= finalY/64+mr.magicSpace;i++){
          for(int j = finalX/64-mr.magicSpace; j <= finalX/64+mr.magicSpace;j++){
            if( j < 0 || j >= column || i < 0 || i >= row )continue;

            if(petStand[i][j] != null && petStand[i][j] != extPet){
              g2d.drawImage(extPet.getHurtImage(mr.atk), j*64-28, i*64-28, 128, 128, this);
              //if(!hurtTimer.isRunning())petStand[i][j].set_HP(petStand[i][j].get_HP()-mr.atk);
            }
              
          }
      }

      }

      Toolkit.getDefaultToolkit().sync();
      g.dispose();

        
    }

    public void magicStart(String cmd){
      finalX = extPet.getX();
      finalY = extPet.getY();
      showOrHideMagicSpace(true, cmd);

      

    }

    public boolean isMagicIncludeEnemy(int originX, int originY){
      //MagicRegion mr = extPet.prepareMagic(cmd);
      //System.err.println(mr.magicSpace);
      for(int i = originY-mr.magicSpace; i <= originY+mr.magicSpace;i++){
        for(int j = originX-mr.magicSpace; j <= originX+mr.magicSpace;j++){
          if( j < 0 || j >= column || i < 0 || i >= row )continue;

          if(petStand[i][j] != null && petStand[i][j] != extPet)return true;
        }
      }
      return false;

    }





	public Map(POOPet [] p, POOArenaExt a) {
    setLayout(null);
    petStand = new POOPetExt[row][column];
    background = new JLabel[row][column];
    moveTimer = new javax.swing.Timer(80, this);
    attackTimer = new javax.swing.Timer(80, this);
    magicTimer = new javax.swing.Timer(80, this);
    hurtTimer = new javax.swing.Timer(80, this);
    setDoubleBuffered(true);
    arena = a;
    try{originalFloor = new ImageIcon(((new ImageIcon("./pic/originalTile.png")).getImage()).getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    catch(Exception e){System.out.println(e);}
    
    try{selectableFloor = new ImageIcon(((new ImageIcon("./pic/movedTile.png")).getImage()).getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    catch(Exception e){System.out.println(e);}

    try{attackableFloor = new ImageIcon(((new ImageIcon("./pic/attackableTile.png")).getImage()).getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    catch(Exception e){System.out.println(e);}

    try{magicableFloor = new ImageIcon(((new ImageIcon("./pic/magicableTile.png")).getImage()).getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    catch(Exception e){System.out.println(e);}
    
    
    mouse = new MouseAdapter(){
      @Override
      public void mouseMoved(MouseEvent e){
        if(arena.getStatus().isMagicClicked() &&  extPet.getX()/64-mr.selecteSpace <= e.getX()/64 && extPet.getX()/64+mr.selecteSpace >= e.getX()/64
          && extPet.getY()/64-mr.selecteSpace <= e.getY()/64 && extPet.getY()/64+mr.selecteSpace >= e.getY()/64  && (finalX/64 != e.getX()/64 || finalY/64 != e.getY()/64)
          && !magicTimer.isRunning() ){
          showOrHideMagicPotentialSpace(false);
          showOrHideMagicSpace(true, null);
          finalX = e.getX();
          finalY = e.getY();
          showOrHideMagicPotentialSpace(true);
           
        }


      }
      
      public void mouseClicked(MouseEvent e){
        
        if(arena.getStatus().isMoveClicked() && background[e.getY()/64][e.getX()/64].getIcon() == selectableFloor && petStand[e.getY()/64][e.getX()/64] == null){
          arena.getStatus().disbaleMove();

          showOrHideMoveSteps(false);
          finalX = e.getX()/64*64;
          finalY = e.getY()/64*64;
          petStand[extPet.getY()/64][extPet.getX()/64] = null;
          nextAnimationFrame = WalkPose.LeftLeg;
          moveTimer.start();

        }else if(arena.getStatus().isAttackClicked() && petStand[e.getY()/64][e.getX()/64] != null && petStand[e.getY()/64][e.getX()/64] != extPet){          
          arena.getStatus().disbaleAttack();
          showOrHideAttackSpace(false);
          finalX = e.getX()/64*64;
          finalY = e.getY()/64*64;
          extPet.attackStart();
          attackTimer.start();


        }else if(arena.getStatus().isMagicClicked() && extPet.getX()/64-mr.selecteSpace <= e.getX()/64 && extPet.getX()/64+mr.selecteSpace >= e.getX()/64
          && extPet.getY()/64-mr.selecteSpace <= e.getY()/64 && extPet.getY()/64+mr.selecteSpace >= e.getY()/64 && isMagicIncludeEnemy(e.getX()/64, e.getY()/64)
          && extPet.isMPEnough(mr.cmd, true)){

          showOrHideMagicSpace(false, null);
          showOrHideMagicPotentialSpace(false);
          finalX = e.getX()/64*64;
          finalY = e.getY()/64*64;
          extPet.magicStart(mr);
          //if(extPet.isMPEnough(magicCommand[i], false))b.setEnabled(false);
          magicTimer.start();


        }
      }



        

    };
    //for(int i = 0;i <row;i++)for(int j = 0;j < column;j++)petStand[i][j] = false;
    pets = p;

    for(int i = 0;i < pets.length;i++){
      extPet = (POOPetExt)pets[i];
      petStand[extPet.getY()/64][extPet.getX()/64] = (POOPetExt)pets[i];
    }

    for(int i = 0;i <row;i++)for(int j = 0;j < column;j++){
      
      JLabel l = new JLabel(originalFloor);
      l.setBounds(j*64, i*64, 64, 64);
      add(l, JLayeredPane.DEFAULT_LAYER);

      background[i][j] = l;

    }
    addMouseListener(mouse);
    addMouseMotionListener(mouse);
    
    }

    public void showOrHideMagicPotentialSpace(boolean isShow){
      int x = finalX/64;
      int y = finalY/64;
      
      for(int i = -mr.magicSpace; i <= mr.magicSpace;i++){
        for(int j = -mr.magicSpace; j <= mr.magicSpace;j++){
          if(x+j < 0 || x+j >= column || (y+i) < 0 || (y+i) >= row )continue;
          

          JLabel l;
          if(isShow)l = new JLabel(magicableFloor);
          else l = new JLabel(originalFloor);          
          l.setBounds((x+j)*64, (y+i)*64, 64, 64);

          this.remove(background[y+i][x+j]);
          add(l, JLayeredPane.PALETTE_LAYER);
          background[y+i][x+j] = l;

        }
      }
      this.repaint();


    }

    public void showOrHideMagicSpace(boolean isShow, String cmd){
      if(cmd != null)mr = extPet.prepareMagic(cmd);
      // System.err.println(mr.magicSpace);

      int x = extPet.getX()/64;
      int y = extPet.getY()/64;
      
      for(int i = -mr.selecteSpace; i <= mr.selecteSpace;i++){
        for(int j = -mr.selecteSpace; j <= mr.selecteSpace;j++){
          if(x+j < 0 || x+j >= column || (y+i) < 0 || (y+i) >= row )continue;
          

          JLabel l;
          if(isShow)l = new JLabel(attackableFloor);
          else l = new JLabel(originalFloor);          
          l.setBounds((x+j)*64, (y+i)*64, 64, 64);

          this.remove(background[y+i][x+j]);
          add(l, JLayeredPane.PALETTE_LAYER);
          background[y+i][x+j] = l;

        }
      }
      this.repaint();

      
    }
    public void showOrHideAttackSpace(boolean isShow){  
      int space = extPet.get_SPACE();
      int x = extPet.getX()/64;
      int y = extPet.getY()/64;
      
      for(int i = -space; i <= space;i++){
        for(int j = -space; j <= space;j++){
          if(x+j < 0 || x+j >= column || (y+i) < 0 || (y+i) >= row )continue;
          

          JLabel l;
          if(isShow)l = new JLabel(attackableFloor);
          else l = new JLabel(originalFloor);          
          l.setBounds((x+j)*64, (y+i)*64, 64, 64);

          this.remove(background[y+i][x+j]);
          add(l, JLayeredPane.PALETTE_LAYER);
          background[y+i][x+j] = l;

        }
      }
      this.repaint();

    }

    public void resetFloor(boolean isMoveClicked, boolean isAttackClicked, boolean isMagicClicked){
      if(isMoveClicked)showOrHideMoveSteps(false);
      if(isAttackClicked)showOrHideAttackSpace(false);
      if(isMagicClicked){
        showOrHideMagicSpace(false, null);
        showOrHideMagicPotentialSpace(false);

      }
    }

    


    public void showOrHideMoveSteps(boolean isShow){
      int agi = extPet.get_AGI();
      int x = extPet.getX()/64;
      int y = extPet.getY()/64;
      
      for(int i = -agi; i <= agi;i++){
        for(int j = -agi; j <= agi;j++){
          if(x+j < 0 || x+j >= column || (y+i) < 0 || (y+i) >= row )continue;
          

          JLabel l;
          if(isShow)l = new JLabel(selectableFloor);
          else l = new JLabel(originalFloor);          
          l.setBounds((x+j)*64, (y+i)*64, 64, 64);

          this.remove(background[y+i][x+j]);
          add(l, JLayeredPane.PALETTE_LAYER);
          background[y+i][x+j] = l;

        }
      }
      this.repaint();

    }

}
