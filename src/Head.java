import ntu.csie.oop13spring.*;

import java.util.*;
import java.util.concurrent.*;
import java.io.*;
import java.lang.*;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Head extends JPanel{
  private Image face;

  public Head(String headImagePath) {
    try{
      face = new ImageIcon(this.getClass().getResource(headImagePath)).getImage();
    }
    catch(Exception e){
      System.out.println(e);

      }
    }
    public void paint(Graphics g) {

        Graphics2D g2d = (Graphics2D) g;
        g2d.drawImage(face, 0, 0, null);
  
    }
}