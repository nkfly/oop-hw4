import ntu.csie.oop13spring.*;

import java.util.*;
import java.util.concurrent.*;
import java.io.*;
import java.lang.*;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


enum Direction {
    Up, Down, Left, Right
}
enum WalkPose {
    Still, LeftLeg, RightLeg
  }




public class POOArenaExt extends POOArena{
  Frame frame;
  Map map;
  Controll [] contorllers;
  POOPet [] pets;
  MyWaitNotify ready;
  ExecutorService executor;

  int turn = 0;
  POOPetExt extPet;


  public POOArenaExt(){
    frame = new Frame(this);
    ready = new MyWaitNotify();
    executor = Executors.newFixedThreadPool(3);


  }

  public void getThreadDoJob(Runnable r){
      executor.execute(r);
  }


  public boolean fight(){// wait for command
    if(pets == null){// second-time initialization
      pets = getAllPets();
      map = new Map(pets, this);
      frame.setMap(map);
      
      for(int i = 0;i < pets.length;i++){
        extPet = (POOPetExt)pets[i];
        extPet.drawSelf(frame.getMap(), this);
        }
        
    }

    int alivePet = 0;
    for(int i = 0;i < pets.length;i++){
      //extPet = (POOPetExt)pets[i];
      if(((POOPetExt)pets[i]).get_HP() > 0){
        alivePet++;
        extPet = (POOPetExt)pets[i];
      }
      else {
        map.remove(((POOPetExt)pets[i]).getIconLabel());
      }
    }
    

    // try {
    //   Thread.sleep(3000);
    //   //wait();
    // } catch(InterruptedException ex) {
    //   Thread.currentThread().interrupt();
    // }


    if(alivePet <= 1){
      //frame.finalize();
      //frame.setVisible(false);

      // JFrame frame = new JFrame();
      // frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      // frame.setSize(896, 666);
      // frame.setLocationRelativeTo(null);
      // frame.setResizable(false);
      // frame.setVisible(true);
      //System.err.println("??");
      map.repaint();
      JOptionPane.showMessageDialog(frame,
    extPet.get_Name() + " wins in the fight",
    "Winner Announcement",
    JOptionPane.PLAIN_MESSAGE);

      return false;
    }
    else {
      Runnable r = new Runnable(){
        public void run(){
          frame.setControll(pets[turn], ready);
          map.setCurrentExtPet(pets[turn]);
        }

      };

      getThreadDoJob(r);

      ready.doWait();

      
      return true;
    }
    
  }
    public void show(){// change to the next pet's controll   
      do {
        turn = (turn+1)%pets.length;
        extPet = (POOPetExt)pets[turn];
      }while(extPet.get_HP() <= 0);
      
      frame.removeControll();

    }

    

    public POOCoordinate getPosition(POOPet p){
      POOPetExt petExt = (POOPetExt)p;
      return petExt.getPosition();
    }


  public Map getMap(){return map;}
  public Status getStatus(){
    return frame.getControll().getStatus();

  }


}























