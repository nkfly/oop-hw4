import ntu.csie.oop13spring.*;

import java.util.*;
import java.io.*;
import java.lang.*;

import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.ImageIO;

public class POOPetCat extends POOPetExt{
	protected File meteorImg = new File("./pic/Meteor.png");
	protected ImageIcon meteor;
	protected File earthImg = new File("./pic/Earth3.png");
	protected ImageIcon earth;
	public POOPetCat(){
		x = 10*64;
		y = 4*64;

		headImagePath = "./pic/cat.png";
		try{front = new ImageIcon(((new ImageIcon("./pic/RYO_front.png")).getImage()).getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    	catch(Exception e){System.out.println(e);}
    	try{front_left = new ImageIcon(((new ImageIcon("./pic/RYO_front_leftleg.png")).getImage()).getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    	catch(Exception e){System.out.println(e);}
    	try{front_right = new ImageIcon(((new ImageIcon("./pic/RYO_front_rightleg.png")).getImage()).getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    	catch(Exception e){System.out.println(e);}
    	try{left = new ImageIcon(((new ImageIcon("./pic/RYO_left.png")).getImage()).getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    	catch(Exception e){System.out.println(e);}
    	try{left_left = new ImageIcon(((new ImageIcon("./pic/RYO_left_leftleg.png")).getImage()).getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    	catch(Exception e){System.out.println(e);}
    	try{left_right = new ImageIcon(((new ImageIcon("./pic/RYO_left_rightleg.png")).getImage()).getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    	catch(Exception e){System.out.println(e);}
    	try{right = new ImageIcon(((new ImageIcon("./pic/RYO_right.png")).getImage()).getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    	catch(Exception e){System.out.println(e);}
    	try{right_left = new ImageIcon(((new ImageIcon("./pic/RYO_right_leftleg.png")).getImage()).getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    	catch(Exception e){System.out.println(e);}
    	try{right_right = new ImageIcon(((new ImageIcon("./pic/RYO_right_rightleg.png")).getImage()).getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    	catch(Exception e){System.out.println(e);}
    	try{back = new ImageIcon(((new ImageIcon("./pic/RYO_back.png")).getImage()).getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    	catch(Exception e){System.out.println(e);}
    	try{back_left = new ImageIcon(((new ImageIcon("./pic/RYO_back_leftleg.png")).getImage()).getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    	catch(Exception e){System.out.println(e);}
    	try{back_right = new ImageIcon(((new ImageIcon("./pic/RYO_back_rightleg.png")).getImage()).getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    	catch(Exception e){System.out.println(e);}
		
		setName("POOPetCat");
		setHP(200);
		setMP(150);
		setAGI(4);
		attack_space = 1;
		attackFrame = 0;
		attackFrameSize = 9;
		attackImg = new File("./pic/Attack7.png");
		ATK = 10;
		magicSkillNumber = 2;
	}


	public MagicRegion prepareMagic(String cmd){
		if(cmd.equals("Meteor")){
			return new MagicRegion(3, 3, 50,cmd);
		}else if(cmd.equals("Earth")){
			return new MagicRegion(2, 2, 300,cmd);

		}
		return null;

	}



	public String [] getMagicCommand(){
		String [] magicCommand = new String[magicSkillNumber];
		magicCommand[0] = new String("Meteor");
		magicCommand[1] = new String("Earth");
		return magicCommand;

	}

	public int magicStart(MagicRegion mr){
		magicSkillFrame++;
		if(mr.cmd.equals("Meteor")){
			magicSkillFrame %= 22;
		}else if(mr.cmd.equals("Earth")){
			magicSkillFrame %= 30;
		}
		return magicSkillFrame;

	}

	public boolean isMPEnough(String cmd, boolean performDecreasement){
		int mpUsage = 0;
		if(cmd.equals("Meteor")){
			mpUsage = 20;
		}else if(cmd.equals("Earth")){
			mpUsage = 40;
		}

		if(getMP() - mpUsage >= 0){
			if(performDecreasement)setMP(getMP()-mpUsage);
			return true;
		}else return false;

	}

	public BufferedImage getMagicImage(MagicRegion mr){
		BufferedImage in = null;
		if(mr.cmd.equals("Meteor")){
			try {
				in = ImageIO.read(meteorImg);
			}
			catch(Exception e){
				System.err.println(e);
			}
			return in.getSubimage((magicSkillFrame-1)%22%5*192, (magicSkillFrame-1)%22/5*192, 192, 192);

		

		}else if(mr.cmd.equals("Earth")){
			try {
				in = ImageIO.read(earthImg);
			}
			catch(Exception e){
				System.err.println(e);
			}
			return in.getSubimage((((magicSkillFrame-1)%30)%5)*192, (((magicSkillFrame-1)%30)/5)*192, 192, 192);


		}
		
		
		return null;

	}

	public String getHeadImagePath(){
		return headImagePath;
	}



	@Override
	public BufferedImage getAttackImage(int hurt){
		
		
		BufferedImage in = null;
		
		try {
			in = ImageIO.read(attackImg);
		}
		catch(Exception e){
			System.err.println(e);
		}
		// BufferedImage newImage;
		// newImage = new BufferedImage(in.getWidth(), in.getHeight(), BufferedImage.TYPE_INT_ARGB);


		 if(attackFrame == 0)return in.getSubimage(192, 192, 192, 192);
		 else if(attackFrame == 1)return in.getSubimage(0, 0, 192, 192);
		 else if(attackFrame == 2)return in.getSubimage(1*192, 0, 192, 192);
		 else if(attackFrame == 3)return in.getSubimage(2*192, 0, 192, 192);
		 else if(attackFrame == 4)return in.getSubimage(3*192, 0, 192, 192);
		 else if(attackFrame == 5)return in.getSubimage(4*192, 0, 192, 192);
		 else if(attackFrame == 6)return in.getSubimage(0, 192, 192, 192);
		 else if(attackFrame == 7)return getHurtImage(hurt);
		 else if(attackFrame == 8)return getHurtImage(hurt);
		 return null;
	}
	


}