import ntu.csie.oop13spring.*;
//package ntu.csie.oop13spring;

import java.io.*;
import java.util.*;
import java.lang.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.imageio.ImageIO;




public abstract class POOPetExt extends POOPet{
	protected int x, y;
	protected int attack_space;
	protected int attackFrame;
	protected int attackFrameSize;
	protected int ATK;
	protected String headImagePath;
	protected String frontImagePath;
	protected Map map;
	protected POOArenaExt arena;
	protected ImageIcon selfIcon;
	protected JLabel iconLabel;
	protected File attackImg;
	protected File walkImg;
	protected File hpImg = new File("./pic/HP-.png");

	ImageIcon front;
	ImageIcon front_left;
	ImageIcon front_right;
	ImageIcon left;
	ImageIcon left_left;
	ImageIcon left_right;
	ImageIcon right;
	ImageIcon right_left;
	ImageIcon right_right;
	ImageIcon back;
	ImageIcon back_left;
	ImageIcon back_right;
	int magicSkillNumber;
	int magicSkillFrame = 0;
	int hurtFrame = 0;


	public abstract String [] getMagicCommand();

	public abstract MagicRegion prepareMagic(String cmd);

	public abstract int magicStart(MagicRegion mr);


	



	public JLabel getIconLabel(){return iconLabel;}

	public int hurtStart(){
		hurtFrame = (hurtFrame+1)%4;
		return hurtFrame;
	}

	public BufferedImage getHurtImage(int hurt){
		int digit = 0;
		int copyHurt = hurt;
		while(copyHurt !=  0 ){
			digit++;
			copyHurt /= 10;
		}

		BufferedImage combined = new BufferedImage(11*digit, 28, BufferedImage.TYPE_INT_ARGB);
		Graphics g = combined.getGraphics();
		for(int i = digit-1; i >= 0;i--){
			try{
				copyHurt = hurt%10;
				g.drawImage(ImageIO.read(hpImg).getSubimage(copyHurt*11, 0, 11, 28), i*11, 0, null);
				hurt /= 10;
			}catch(Exception e){
				System.err.println(e);
			}

		}
		return combined;

	}

	public void turnAroundEnd(){
		selfIcon = front;
		iconLabel = new JLabel(selfIcon);
		iconLabel.setBounds(x, y, 64, 64);
		
		map.add(iconLabel, JLayeredPane.MODAL_LAYER);
		map.repaint();
		

	}

	public void drawSelf(Map m, POOArenaExt a){// second-time  initializatoin
		selfIcon = front;
		iconLabel = new JLabel(selfIcon);

		iconLabel.setBounds(x, y, 64, 64);

		arena = a;
		map = m;
		map.add(iconLabel, JLayeredPane.MODAL_LAYER);
		map.repaint();
		

	}

	public abstract boolean isMPEnough(String cmd, boolean performDecreasement);
	

	

	public int attackStart(){
		attackFrame = (attackFrame+1)%attackFrameSize;
		return attackFrame;
	}

	public abstract BufferedImage getMagicImage(MagicRegion mr);

	public abstract BufferedImage getAttackImage(int hurt);
		
	

	 public ImageIcon getSelfIcon(){
	 	return selfIcon;
	 }

	 public ImageIcon walk(Direction d, WalkPose p){
	 	if(d == Direction.Up && p == WalkPose.Still){
	 		selfIcon = back;
	 		y = y- 22;
	 	}
	 	else if(d == Direction.Up && p == WalkPose.LeftLeg){
	 		selfIcon = back_left;
	 		y = y - 21;
	 	}
	 	else if(d == Direction.Up && p == WalkPose.RightLeg){
	 		selfIcon = back_right;
	 		y = y - 21;	 	
	 	}
	 	else if(d == Direction.Down && p == WalkPose.Still){
	 		selfIcon  = front;
	 		y = y+22;
	 		//selfIcon.setBounds(x*64, y*64, 64, 64);
	 	}
	 	else if(d == Direction.Down && p == WalkPose.LeftLeg){
	 		selfIcon = front_left;
	 		y = y + 21;
	 		//selfIcon.setBounds(x*64, y*64+21, 64, 64);
	 	}
	 	else if(d == Direction.Down && p == WalkPose.RightLeg){
	 		selfIcon = front_right;
	 		y += 21;
	 		//selfIcon.setBounds(x*64, y*64+42, 64, 64);
	 	}
	 	else if(d == Direction.Left && p == WalkPose.Still){
	 		selfIcon = left;
	 		x = x-22;
	 		//selfIcon.setBounds(x*64, y*64, 64, 64);

	 	}
	 	else if(d == Direction.Left && p == WalkPose.LeftLeg){
	 		selfIcon = left_left;
	 		x -= 21;
	 		//selfIcon.setBounds(x*64-21, y*64, 64, 64);

	 	}
	 	else if(d == Direction.Left && p == WalkPose.RightLeg){
	 		selfIcon = left_right;
	 		x -= 21;
	 		//selfIcon.setBounds(x*64-42, y*64, 64, 64);

	 	}else if(d == Direction.Right && p == WalkPose.Still){
	 		selfIcon = right;
	 		x = x+22;
	 		//selfIcon.setBounds(x*64, y*64, 64, 64);

	 	}
	 	else if(d == Direction.Right && p == WalkPose.LeftLeg){
	 		selfIcon = right_left;
	 		x += 21;

	 	}
	 	else if(d == Direction.Right && p == WalkPose.RightLeg){
	 		selfIcon = right_right;
	 		x += 21;
	 		

	 	}
	 	
	 	
	 	return selfIcon;



	 }




	public int getX(){
		return x;
	}

	public int getY(){
		return y;
	}

	

	public abstract String getHeadImagePath();

    
    public String get_Name(){
        return getName();
    }

    public int get_SPACE(){
    	return attack_space;

    }
    
    public int get_AGI(){
        return getAGI();
    }

    public int get_MP(){
        return getMP();
    }
    
    public int get_HP(){
        return getHP();
    }

    public boolean set_HP(int hp){
    	if(hp < 0)return setHP(0);
    	else return setHP(hp);
        
    }

    public int get_ATK(){
    	return ATK;
    }

	public POOCoordinate getPosition(){
		return new POOCoordinateExt(x, y);


	}
	public POOAction act(POOArena arena){
		return new POOAction();

	}

    /**
       move defines how the pet would want to move in an arena;
       note that the range of moving should be related to AGI
     */
    public POOCoordinate move(POOArena arena){
    	return new POOCoordinateExt(x, y);


    }


}