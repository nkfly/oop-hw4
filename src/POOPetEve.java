import ntu.csie.oop13spring.*;

import java.util.*;
import java.io.*;
import java.lang.*;

import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.ImageIO;

class MagicRegion{
	public int selecteSpace;
	public int magicSpace;
	public int atk;
	public String cmd;
	public MagicRegion(int s, int m, int a, String ss){
		selecteSpace = s;
		magicSpace = m;
		atk = a;
		cmd = ss;
	}

}

public class POOPetEve extends POOPetExt{
	protected File frozenLandImg = new File("./pic/Ice5.png");
	protected ImageIcon frozenLand;
	protected File lighteningImg = new File("./pic/Thunder1.png");
	protected ImageIcon lightening;

	public POOPetEve(){
		x = 4*64;
		y = 2*64;

		headImagePath = "./pic/eve.png";
		walkImg = new File("./pic/eve_pose.png");

		BufferedImage in = null;
		
		try {
			in = ImageIO.read(walkImg);
		}
		catch(Exception e){
			System.err.println(e);
		}
		// BufferedImage newImage;
		// newImage = new BufferedImage(in.getWidth(), in.getHeight(), BufferedImage.TYPE_INT_ARGB);


		 


		try{front = new ImageIcon(  in.getSubimage( 32, 0,32,32) .getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    	catch(Exception e){System.out.println(e);}
    	try{front_left = new ImageIcon( in.getSubimage( 0, 0,32,32) .getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)  );}
    	catch(Exception e){System.out.println(e);}
    	try{front_right = new ImageIcon(in.getSubimage( 64, 0,32,32) .getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    	catch(Exception e){System.out.println(e);}
    	try{left = new ImageIcon(in.getSubimage( 32, 32,32,32) .getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    	catch(Exception e){System.out.println(e);}
    	try{left_left = new ImageIcon( in.getSubimage( 0, 32,32,32) .getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)  );}
    	catch(Exception e){System.out.println(e);}
    	try{left_right = new ImageIcon(in.getSubimage( 64, 32,32,32) .getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    	catch(Exception e){System.out.println(e);}
    	try{right = new ImageIcon(in.getSubimage( 32, 64,32,32) .getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)  );}
    	catch(Exception e){System.out.println(e);}
    	try{right_left = new ImageIcon(in.getSubimage( 0, 64,32,32) .getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    	catch(Exception e){System.out.println(e);}
    	try{right_right = new ImageIcon(in.getSubimage( 64, 64,32,32) .getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    	catch(Exception e){System.out.println(e);}
    	try{back = new ImageIcon(in.getSubimage( 32, 96,32,32) .getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    	catch(Exception e){System.out.println(e);}
    	try{back_left = new ImageIcon(in.getSubimage( 0, 96,32,32) .getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    	catch(Exception e){System.out.println(e);}
    	try{back_right = new ImageIcon(in.getSubimage( 64, 96,32,32) .getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)   );}
    	catch(Exception e){System.out.println(e);}
		
		setName("POOPetEve");
		setHP(280);
		setMP(160);
		setAGI(2);
		attack_space = 2;
		attackFrame = 0;
		attackFrameSize = 7;
		attackImg = new File("./pic/Thunder3.png");
		ATK = 30;
		magicSkillNumber = 2;

		

	}

	public MagicRegion prepareMagic(String cmd){
		if(cmd.equals("Frozen Land")){
			return new MagicRegion(3, 1, 35,cmd);
		}else if(cmd.equals("Lightening")){
			return new MagicRegion(5, 2, 50,cmd);

		}
		return null;

	}

	public String [] getMagicCommand(){
		String [] magicCommand = new String[magicSkillNumber];
		magicCommand[0] = new String("Frozen Land");
		magicCommand[1] = new String("Lightening");
		return magicCommand;

	}

	public int magicStart(MagicRegion mr){
		magicSkillFrame++;
		if(mr.cmd.equals("Frozen Land")){
			magicSkillFrame %= 23;
		}else if(mr.cmd.equals("Lightening")){
			magicSkillFrame %= 12;
		}
		return magicSkillFrame;

	}

	public boolean isMPEnough(String cmd, boolean performDecreasement){
		int mpUsage = 0;
		if(cmd.equals("Frozen Land")){
			mpUsage = 20;
		}else if(cmd.equals("Lightening")){
			mpUsage = 40;
		}

		if(getMP() - mpUsage >= 0){
			if(performDecreasement)setMP(getMP()-mpUsage);
			return true;
		}else return false;

	}

	public BufferedImage getMagicImage(MagicRegion mr){
		BufferedImage in = null;
		if(mr.cmd.equals("Frozen Land")){
			try {
				in = ImageIO.read(frozenLandImg);
			}
			catch(Exception e){
				System.err.println(e);
			}
			return in.getSubimage((magicSkillFrame-1)%23%5*192, (magicSkillFrame-1)%23/5*192, 192, 192);

		

		}else if(mr.cmd.equals("Lightening")){
			try {
				in = ImageIO.read(lighteningImg);
			}
			catch(Exception e){
				System.err.println(e);
			}
			return in.getSubimage((((magicSkillFrame-1)%12)%5)*192, (((magicSkillFrame-1)%12)/5)*192, 192, 192);


		}
		
		
		return null;

	}

	@Override
	public BufferedImage getAttackImage(int hurt){
		
		
		BufferedImage in = null;
		
		try {
			in = ImageIO.read(attackImg);
		}
		catch(Exception e){
			System.err.println(e);
		}
		// BufferedImage newImage;
		// newImage = new BufferedImage(in.getWidth(), in.getHeight(), BufferedImage.TYPE_INT_ARGB);


		 if(attackFrame == 0)return in.getSubimage(192, 192, 192, 192);
		 else if(attackFrame == 1)return in.getSubimage(0, 0, 192, 192);
		 else if(attackFrame == 2)return in.getSubimage(1*192, 0, 192, 192);
		 else if(attackFrame == 3)return in.getSubimage(2*192, 0, 192, 192);
		 else if(attackFrame == 4)return in.getSubimage(3*192, 0, 192, 192);
		 else if(attackFrame == 5)return getHurtImage(hurt);
		 else if(attackFrame == 6)return getHurtImage(hurt);
		 return null;
	}





	public String getHeadImagePath(){
		return headImagePath;
	}
	


}