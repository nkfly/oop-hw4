import ntu.csie.oop13spring.*;

import java.util.*;
import java.util.concurrent.*;
import java.io.*;
import java.lang.*;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;



public class Status extends JPanel implements ActionListener{
  POOPetExt extPet;
  JLabel barHPMP;
  
  JLabel name;
  JButton move;
  JButton attack;
  JButton rest;
  //JButton magic;
  MagicControll magic;
  MyWaitNotify ready;
  Map map;
  boolean isMoving = false;
  boolean isAttacking = false;
  boolean isMagicing = false;
  

  private void resetButton(){
    isMoving = false;
    isAttacking = false;
    isMagicing = false;
    

  }

  public Status(POOPet pet, MyWaitNotify r, Map m){
    super(new GridLayout(3,2));

    ready = r;
    extPet = (POOPetExt)pet;
    map = m;
    barHPMP = new JLabel("HP:" + extPet.get_HP()+"         MP: " + extPet.get_MP() + "         ATK: " +extPet.get_ATK() + "         AGI:" + extPet.get_AGI());
    
    name = new JLabel(extPet.get_Name());

    move = new JButton("move");
    move.setActionCommand("move");
    move.addActionListener(this);

    attack = new JButton("attack");
    attack.setActionCommand("attack");
    attack.addActionListener(this);

    magic = new MagicControll(this);
    magic.setVisible(true);
    //magic.setActionCommand("magic");
    //magic.addActionListener(this);

    rest = new JButton("rest");
    rest.setActionCommand("rest");
    rest.addActionListener(this);

    add(name);
    add(barHPMP);
    add(move);
    add(attack);
    add(magic);
    add(rest);

  }

  public boolean isMoveClicked(){return isMoving;}
  public boolean isAttackClicked(){return isAttacking;}
  public boolean isMagicClicked(){return isMagicing;}
  public void notifyMainThread(){
        ready.doNotify();
  }
  

  public void disbaleMove(){
    move.setEnabled(false);
  }
  public void disbaleAttack(){
    attack.setEnabled(false);
  }

  public void setIsMagicing(boolean s){
    isMagicing = s;
  }




  public void actionPerformed(ActionEvent e){
    String cmd = e.getActionCommand();
      if (cmd == "move") {
        // if(isAttacking)map.showOrHideAttackSpace(false);
        // if(isMagicing)map.showOrHideMagicSpace(false, null);
        map.resetFloor(isMoving, isAttacking, isMagicing);
        resetButton();
        isMoving = true;
        map.showOrHideMoveSteps(true);

      }else if (cmd == "attack") {
        // if(isMoving)map.showOrHideMoveSteps(false);
        // if(isMagicing)map.showOrHideMagicSpace(false, null);
        map.resetFloor(isMoving, isAttacking, isMagicing);
        resetButton();
        isAttacking = true;
        map.showOrHideAttackSpace(true);
        //ready.doNotify();
      }else if (cmd == "rest") {
        map.resetFloor(isMoving, isAttacking, isMagicing);
        resetButton();
        ready.doNotify();
      }
      

  }

  public void checkMPEnough(){
     for(int i = 0 ;i < magic.magicCommand.length;i++){
      if(!extPet.isMPEnough(magic.magicCommand[i], false))magic.magicButton[i].setEnabled(false);
      //magic.magicButton[i].setEnabled(false);
     }
  }

  class MagicControll extends JPanel implements ActionListener{
  JButton upper;
  String [] magicCommand;
  JButton [] magicButton;
  Status containerStatus;
  public MagicControll(Status s){
    BorderLayout layout = new BorderLayout();
    setLayout(layout);
    containerStatus = s;

    upper = new JButton("magic");
    upper.setActionCommand("magic");
    upper.addActionListener(this);

    magicCommand = extPet.getMagicCommand();
    //magicButton = new JButton[magicCommand.length];

    add(upper);


  }

  public void actionPerformed(ActionEvent e){
    String cmd = e.getActionCommand();
    if(cmd.equals("magic")){
      GridLayout layout = new GridLayout((magicCommand.length+1)/2, 2);
      setLayout(layout);
      remove(upper);

      for(int i = 0;i < magicCommand.length;i++){
        JButton b = new JButton(magicCommand[i]);
        b.setActionCommand(magicCommand[i]);
        b.addActionListener(this);
        //magicButton[i] = b;
        //if(!extPet.isMPEnough(magicCommand[i], false))b.setEnabled(false);
        add(b);
        if(!extPet.isMPEnough(magicCommand[i], false))b.setEnabled(false);



      }
      repaint();
      revalidate();


    }else {
      //extPet.prepareMagic(cmd);
      //if(containerStatus.isMagicClicked())map.showOrHideMagicSpace(false, null);
      map.resetFloor(isMoving, isAttacking, isMagicing);
      containerStatus.setIsMagicing(true);
      map.magicStart(cmd);
      //if(extPet.isMPEnough(magicCommand[i], false))b.setEnabled(false);
    }

  }



  // public void paint(Graphics g) {
  //       super.paint(g);
      

        
  //   }

}


}