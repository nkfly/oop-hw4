import ntu.csie.oop13spring.*;

import java.util.*;
import java.util.concurrent.*;
import java.io.*;
import java.lang.*;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;



public class Controll extends JPanel {

  POOPetExt extPet;
  Status s;
  Head h;
  POOArenaExt arena;

  public Status getStatus(){
    return s;
  }

  public Controll(POOPet p, MyWaitNotify ready, POOArenaExt a){
    super(new BorderLayout());
    extPet = (POOPetExt)p;
    arena = a;

    setBackground(Color.LIGHT_GRAY);

    //
    s = new Status(p, ready, arena.getMap());
    add(s, BorderLayout.CENTER);
    s.setPreferredSize(new Dimension(200, 10));
    //System.err.println(extPet.getHeadImagePath());

    h = new Head(extPet.getHeadImagePath());
    h.setPreferredSize(new Dimension(200, 10));
    add(h, BorderLayout.WEST);

  }

}